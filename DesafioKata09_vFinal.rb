REGRAS = Hash.new

#Promoções para A
REGRAS['A'] = Array.new
REGRAS['A'][1] = 50
#REGRAS['A'][2] = 90
REGRAS['A'][3] = 130
#REGRAS['A'][5] = 170

#Promoções para B
REGRAS['B'] = Array.new
REGRAS['B'][1] = 30
REGRAS['B'][2] = 45


#Promoções para C
REGRAS['C'] = Array.new
REGRAS['C'][1] = 20

#Promoções para D
REGRAS['D'] = Array.new
REGRAS['D'][1] = 15

COMPRA = "DABABAABBC"


class CheckOut
 #Valores iniciais
  def initialize(promocoes)
    @regras = promocoes
    @produtos = Hash.new
  end

 #Escaneia cada item 
  def scan(item)
    @produtos[item] ||= 0
    @produtos[item] += 1
  end

 #Calcula o total
  def total
    @subtotal = 0
    @produtos.each_key do |item|     
       itens_restantes = @produtos[item]
       @regras[item].reverse_each do |preco_promo|
         next if preco_promo == nil
         qtd_itens_promo = @regras[item].find_index(preco_promo)
         qtd_promo, sobra = itens_restantes.divmod(qtd_itens_promo)
     @subtotal += qtd_promo * preco_promo
     itens_restantes = sobra
       end 
    end
    return @subtotal
  end

end

def preco(compras = COMPRA)
  carrinho = CheckOut.new(REGRAS)
  compras.split(//).each do |item|
    carrinho.scan(item) 
  end

 #Exibe o valor total da compra  
 #  puts carrinho.total
end

#Funcao de preco
preco
