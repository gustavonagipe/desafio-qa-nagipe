Feature: Encaminhar arquivos recebidos dentro de um chat para outro contato 
Como usuário do WhatsApp 
Eu quero poder encaminhar arquivos recebidos dentro de um chat para outro contato
Então o meu outro contato receberá também o(s) arquivo(s)

Scenario: Usuário encaminha imagem recebida pelo chat de um contato para o outro

	Given Eu estou em um chat com um contato
	When Eu clico na imagem recebida ""<nome_foto>"
		And Eu clico no botão "Encaminhar"
		And Eu seleciono o contato "<nome_contato>"
		And Eu clico no botão "Enviar"
	Then O arquivo é enviado para o contato

	Examples:
			| nome_foto   |
			| img_01.jpeg |
			| img_03.jpeg |
			
			| nome_contato |
			| Jaqueline    |
			| Antonio      |
			| Fabio        |
			
			
Scenario: Usuário encaminha vídeo recebido pelo chat de um contato para o outro

	Given Eu estou em um chat com um contato
	When Eu clico na vídeo recebido "<nome_video>"
		And Eu clico no botão "Encaminhar"
		And Eu seleciono o contato "<nome_contato>"
		And Eu clico no botão "Enviar"
	Then O arquivo é enviado para o contato

	Examples:
			| nome_video  |
			| vid_01.mp4  |
			| vid_02.mp4  |
			| vid_03.mp4  |
			
			| nome_contato |
			| Jaqueline    |
			| Antonio      |
			| Fabio        |
			
			
Scenario: Usuário encaminha múltiplos arquivos recebidos pelo chat de um contato para o outro

	Given Eu estou em um chat com um contato
	When Eu seleciono o arquivo recebido "<nome_arquivo>"
		And Eu clico em outro arquivo recebido "<nome_arquivo>"
		And Eu clico em outro arquivo recebido "<nome_arquivo>"
		And Eu clico no botão "Encaminhar"
		And Eu seleciono o contato "<nome_contato>"
		And Eu clico no botão "Enviar"
	Then O arquivo é enviado para o contato

	Examples:
			| nome_arquivo |
			| img_01.jpeg  |
			| img_02.jpeg  |
			| img_03.jpeg  |
			| vid_01.mp4   |
			| vid_02.mp4   |
			| vid_03.mp4   |
			
			| nome_contato |
			| Jaqueline    |
			| Antonio      |
			| Fabio        |