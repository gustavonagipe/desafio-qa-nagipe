Feature: Enviar arquivos da galeria para outro contato dentro de um chat
Como usuário do WhatsApp através da plataforma Android
Eu quero poder enviar arquivos da galeria para outro contato dentro de um chat
Então o meu contato receberá o(s) arquivo(s)

Scenario: Usuário compartilha imagem da galeria com um contato

	Given Eu estou em um chat com um contato
	When Eu clico no botão "EnviarArquivo"
		And Eu seleciono a opção "Galeria"
		And Eu seleciono a opção "Fotos"
		And Eu seleciono a opção "Todas as fotos"
		And Eu seleciono a foto "<nome_foto>"
		And Eu clico no botão "Enviar"
	Then O arquivo é enviado para o contato

	Examples:
			| nome_foto   |
			| img_01.jpeg |
			| img_03.jpeg |


Scenario: Usuário compartilha mais de uma imagem diferente da galeria com um contato

	Given Eu estou em um chat com um contato
	When Eu clico no botão "EnviarArquivo"
		And Eu seleciono a opção "Galeria"
		And Eu seleciono a opção "Fotos"
		And Eu seleciono a opção "Todas as fotos"
		And Eu seleciono a foto "<nome_foto>"
		And Eu clico no botão "+"
		And Eu seleciono a foto "<nome_foto>"
		And Eu clico no botão "OK"
		And Eu clico no botão "Enviar"
	Then Os arquivos são enviados para o contato

	Examples:
			| nome_foto   |
			| img_01.jpeg |
			| img_02.jpeg |
			| img_03.jpeg |

	
Scenario: Usuário compartilha video da galeria com um contato

	Given Eu estou em um chat com um contato
	When Eu clico no botão "EnviarArquivo"
		And Eu seleciono a opção "Galeria"
		And Eu seleciono a opção "Vídeos"
		And Eu seleciono a opção "Todos os vídeos"
		And Eu seleciono o vídeo "<nome_video>"
		And Eu clico no botão "Enviar"
	Then O arquivo é enviado para o contato
	
	Examples:
			| nome_video  |
			| vid_01.mp4  |
			| vid_02.mp4  |
			| vid_03.mp4  |