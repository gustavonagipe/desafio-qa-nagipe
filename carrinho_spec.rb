require 'test/unit'
require '../projetos/DesafioKata09_vFinal.rb'

class TestePreco < Test::Unit::TestCase
	def preco(compras = COMPRA)
		carrinho = CheckOut.new(REGRAS)	
		compras.split(//).each do |item|
			carrinho.scan(item) 
		end
	carrinho.total
	end

    def teste_total
		assert_equal(  0, preco(""))
		assert_equal( 50, preco("A"))
        assert_equal( 80, preco("AB"))
        assert_equal(115, preco("CDBA"))	
		
		assert_equal(100, preco("AA"))
        assert_equal(130, preco("AAA"))
        assert_equal(180, preco("AAAA"))
        assert_equal(230, preco("AAAAA"))
        assert_equal(260, preco("AAAAAA"))

        assert_equal(160, preco("AAAB"))
        assert_equal(175, preco("AAABB"))
        assert_equal(190, preco("AAABBD"))
        assert_equal(190, preco("DABABA"))
    end

    def teste_incremental
        carrinho = CheckOut.new(REGRAS)
        assert_equal( 0, carrinho.total)
        carrinho.scan("A"); assert_equal( 50, carrinho.total)
        carrinho.scan("B"); assert_equal( 80, carrinho.total)
        carrinho.scan("A"); assert_equal(130, carrinho.total)
        carrinho.scan("A"); assert_equal(160, carrinho.total)
        carrinho.scan("B"); assert_equal(175, carrinho.total)
    end 
end